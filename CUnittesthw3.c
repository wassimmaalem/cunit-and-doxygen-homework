/*
 * CUnittesthw3.c
 *
 *  Created on: Apr 3, 2020
 *      Author: wassim
 */


#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

#include "Cunittesthw3.h"
#define PERSON NUM 15
#define MALLOC(ptr,size) \
do {                 \
ptr=malloc(size); \
	if(!ptr)
       abort();
}while(0)
#define FREE(ptr)\
	do {			\
		free(ptr);	\
	ptr=NULL;	\

}while (0)
	char PERSON_NAME_PREFIX[]="very long name";
int init_test(void) {
	person_init();
	return(0);
}
int close_test(void){
	person_free();
	return(0);

}
void create_person_test(void){
	struct person_t * p =person_create("CT1",42,cool);
	CU_ASSERT_PTR_NOT_NULL(p);
	CU_ASSERT_EQUAL(strcmp("CT1",p->name),0);
	CU_ASSERT_EQUAL(p->level,cool);
	CU_ASSERT_EQUAL(p->age,42);
}

void add_person_test(void) {
	for(int i=0 ;i<PERSON_NUMBER;I++)
	{char *name=NULL;
	asprintf(&name , " %s %d",PERSON_NAME_PREFIX,i)
	struct person_t *p=person_create(name,(unit8_t)i+1,(enum profession_level)(i%unknown));
	CU_ASSERT_PTR_NOT_NULL(p);
	CU_ASSERT_EQUAL(p->age,i+1);
		CU_ASSERT_EQUAL(p->level,i%unkown);
		CU_ASSERT_EQUAL(person_add(p),EXIT_SUCCESS);
		CU_ASSERT_EQUAL(strcmp(name,p->name),0);
	}
}
void find_first_person_by_level_test(void){
	for(enum profession_level i=newby;i<unknown ;i++)
	{
		strucct peron_t *p=person_find_by_level(i);
		CU_ASSERT_PTR_NOT_NULL(p);

	}

}
void find_first_person_by_age_test(void) {
	for (int i=0 ;i<PERSON_NUMBER;i++){
		struct person_t *p=person_find_by_age(i+1);
		CU_ASSERT_PTR_NOT_NULL(p);
	}

}
int main(void){
	CU_pSuite pSuite=NULL;
	if(CUE_SUCCESS!=CU_initialize_registry())
		return(CU_get_error());

	pSuite=CU_add_suite("SUITE 1",init_test,close_test);
	if (NULL==pSuite){
		CU_cleanup_registry();
		return (CU_get_errors());

	}
}
if ((NULL==CU_add_test(pSuite,"Create person test",create_person_test)))

	(NULL==CU_add_test(pSuite,"Add person test", add_person_test))
	(NULL==CU_add_test(pSuite,"find first person by level",find_first_person_by_level_test))
	(NULL==CU_add_test(pSuite,"find first person by age", find first person by age test))
	CU_cleanup_registry();
	return(CU_get_error());
	}
	CU_basic_set_mode(CU_BRM_VERBOSE);
	CU_BASIC_RUN_TESTS();
	CU_cleanup_registry();
	return(CU_get_error());
	}




